#include <iostream>
#include <fstream>

using std::ifstream;
using std::ofstream;

template<typename T>
class matrix {
public:
    matrix(size_t n, size_t m) : n(n), m(m), mat(new T[n * m]) {

    }

    matrix(ifstream& input) : mat(nullptr) {
        input.read(reinterpret_cast<char*>(&n), sizeof(n));
        input.read(reinterpret_cast<char*>(&m), sizeof(m));

        mat = new T[n * m];

        input.read(reinterpret_cast<char*>(mat), n * m * sizeof(T));
    }

    void output(ofstream& output) {
        output.write(reinterpret_cast<char*>(&n), sizeof(n));
        output.write(reinterpret_cast<char*>(&m), sizeof(m));
        output.write(reinterpret_cast<char*>(mat), n * m * sizeof(T));
    }

    size_t n;
    size_t m;

    T* mat;
};

template<typename T>
matrix<T> matmult_ijk(const matrix<T>& a, const matrix<T>& b) {
    matrix<T> c(a.n, b.m);

    for (int i = 0; i < a.n; ++i) {
        for (int j = 0; j < b.m; ++j) {
            c.mat[i * a.n + j] = 0;

            for (int k = 0; k < a.m; ++k) {
                c.mat[i * a.n + j] += a.mat[i * a.n + k] * b.mat[k * b.n + j];
            }
        }
    }

    return c;
}

template<typename T>
void performMultOnFiles(ifstream& matAfile, ifstream& matBfile,
                        ofstream& matCfile, char type) {
    matrix<T> matA(matAfile);
    matrix<T> matB(matBfile);

    matAfile.close();
    matBfile.close();

    if (matA.m != matB.n) {
        std::cout << "Matrices sizes mismatch" << std::endl;
    }

    matrix<T> matC = matmult_ijk(matA, matB);

    matCfile.write(reinterpret_cast<char*>(&type), sizeof(type));
    matC.output(matCfile);
}

int main(int argc, char** argv) {
    if (argc < 4) {
        std::cout << "Usage: " << argv[0] <<
                " matA matB matC (ijk)" << std::endl;
        return -1;
    }


    ifstream matAfile(argv[1]);
    if (!matAfile.is_open()) {
        std::cout << "Can't open file '" << argv[1] << "'" << std::endl;
        return -1;
    }

    ifstream matBfile(argv[2]);
    if (!matBfile.is_open()) {
        std::cout << "Can't open file '" << argv[2] << "'" << std::endl;
        return -1;
    }

    ofstream matCfile(argv[3]);
    if (!matCfile.is_open()) {
        std::cout << "Can't open file '" << argv[3] << "'" << std::endl;
        return -1;
    }


    char typeA;
    char typeB;
    matAfile.read(reinterpret_cast<char*>(&typeA), sizeof(typeA));
    matBfile.read(reinterpret_cast<char*>(&typeB), sizeof(typeB));

    if (typeA != typeB) {
        std::cout << "Type mismatch" << std::endl;
        return -1;
    }

    if (typeA == 'f') {
        performMultOnFiles<float>(matAfile, matBfile, matCfile, typeA);
    } else if (typeA == 'd') {
        performMultOnFiles<double>(matAfile, matBfile, matCfile, typeA);
    } else {
        std::cout << "Wrong element type" << std::endl;
    }

    matCfile.close();


    return 0;
}
